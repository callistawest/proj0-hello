# Proj0-Hello
-------------

Trivial project to exercise version control, turn-in, and other
mechanisms.

Callista West
cwest10@uoregon.edu
Project 0, CIS 322


hello.py will print "Hello world" to the terminal.
The message "Hello world" is found in the file credentials.ini.
These files are commited to bitbucket using git.

The file is run using a Makefile, and can be ran in the terminal by using the command "make run".

The printed message can vary by changing the message in the file credentials.ini.

    ```
